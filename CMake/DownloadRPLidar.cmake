configure_file("${CMAKE_CURRENT_LIST_DIR}/RPLidarExternalProject.cmake.in"
  "${CMAKE_BINARY_DIR}/rplidar_sdk-download/CMakeLists.txt"
  @ONLY)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/rplidar_sdk-download")
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/rplidar_sdk-download")

set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH}" "${CMAKE_BINARY_DIR}/rplidar_sdk-src")
