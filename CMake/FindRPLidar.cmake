# Find RPLidar library
# Defines:
#  RPLIDAR_FOUND
#  The target RPLidar::RPLidar
#  RPLIDAR_INCLUDE_DIRS
#  RPLIDAR_LIBRARIES

find_path(RPLIDAR_INCLUDE_DIR
  NAMES rplidar.h
  PATH_SUFFIXES
    include
    sdk/sdk/include
  )
find_path(RPLIDAR_SRC_DIR
  NAMES hal/types.h
  PATH_SUFFIXES
    include
    sdk/sdk/src
  )
find_library(RPLIDAR_LIBRARY
  NAMES rplidar_sdk
  PATH_SUFFIXES
    sdk/output/Linux/Release
    sdk/output/win32/Release
    sdk/output/Linux/Debug
    sdk/output/win32/Debug
  )

macro(_rplidar_get_version)
    file(READ "${RPLIDAR_INCLUDE_DIR}/rplidar.h" _rplidar_header)
    string(REGEX MATCH "#define[ \t]+RPLIDAR_SDK_VERSION[ \t]+\"([0-9.]+)\"" _rplidar_version_match "${_rplidar_header}")

    set(RPLIDAR_VERSION "${CMAKE_MATCH_1}")
endmacro(_rplidar_get_version)

if (RPLIDAR_INCLUDE_DIR)
  _rplidar_get_version()
endif(RPLIDAR_INCLUDE_DIR)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(RPLidar
  REQUIRED_VARS RPLIDAR_INCLUDE_DIR RPLIDAR_SRC_DIR RPLIDAR_LIBRARY
  VERSION_VAR RPLIDAR_VERSION)

if (RPLIDAR_FOUND)
  if (NOT RPLIDAR_INCLUDE_DIRS)
    set(RPLIDAR_INCLUDE_DIRS "${RPLIDAR_INCLUDE_DIR}" "${RPLIDAR_SRC_DIR}")
  endif(NOT RPLIDAR_INCLUDE_DIRS)

  if (NOT RPLIDAR_LIBRARIES)
    set(RPLIDAR_LIBRARIES "${RPLIDAR_LIBRARY}")
  endif(NOT RPLIDAR_LIBRARIES)

  if (NOT TARGET RPLidar::RPLidar)
    add_library(RPLidar::RPLidar UNKNOWN IMPORTED)
    set_target_properties(RPLidar::RPLidar PROPERTIES
      IMPORTED_LOCATION "${RPLIDAR_LIBRARIES}"
      INTERFACE_INCLUDE_DIRECTORIES "${RPLIDAR_INCLUDE_DIRS}"
      INTERFACE_LINK_LIBRARIES Threads::Threads)
  endif(NOT TARGET RPLidar::RPLidar)
endif(RPLIDAR_FOUND)
