#include <array>
#include <iostream>
#include <string_view>
#include <memory>

#include <rplidar.h>

namespace
{

const std::array<uint32_t, 3> baudRates { 1000000, 256000, 115200 };

void usage(char const *progname = "rplidar_get_serial")
{
    std::clog << "Usage:\n" << progname << " DEVICE [--get-firmware]\n\n"
              << "Example: " << progname << " /dev/ttyUSB0" << std::endl;
}

class RPlidarDriverDeleter
{
public:
    void operator()(rp::standalone::rplidar::RPlidarDriver *driver)
    {
        rp::standalone::rplidar::RPlidarDriver::DisposeDriver(driver);
    }
};

using RPlidarPtr = std::unique_ptr<rp::standalone::rplidar::RPlidarDriver, RPlidarDriverDeleter>;

}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        usage(argc >= 1 ? argv[0] : "rplidar_get_serial");
        return 2;
    }

    char const *devicePath = nullptr;
    bool getFirmware = false;

    for (int argn = 1; argn < argc; ++argn)
    {
        std::string_view arg = argv[argn];
        if (arg == "--get-firmware")
            getFirmware = true;
        else
            devicePath = argv[argn];
    }

    if (devicePath == nullptr)
    {
        usage(argv[0]);
        return 2;
    }

    for (auto baudRate: baudRates)
    {
        RPlidarPtr drv { rp::standalone::rplidar::RPlidarDriver::CreateDriver(rp::standalone::rplidar::DRIVER_TYPE_SERIALPORT) };
        if (!drv)
        {
            std::cerr << "Could not create driver instance" << std::endl;
            return 1;
        }

        u_result connect_result;
        connect_result = drv->connect(devicePath, baudRate);
        if(IS_OK(connect_result))
        {
            rplidar_response_device_info_t devinfo;
            u_result op_result = drv->getDeviceInfo(devinfo);

            if (IS_OK(op_result))
            {
                std::cout << "serialNum=" << std::hex;
                for (std::size_t i = 0; i < sizeof(devinfo.serialnum); ++i)
                    std::cout << static_cast<unsigned short>(devinfo.serialnum[i]);
                std::cout << std::endl;

                if (getFirmware)
                    std::cout << devinfo.firmware_version << std::endl;
                return 0;
            }
            else
            {
                std::clog << "Could not get device info with baudRate " << baudRate << ": " << op_result << std::endl;
            }

            drv->disconnect();
        }
        else
        {
            std::clog << "Could not connect with baudRate " << baudRate << ": " << connect_result << std::endl;
        }
    }

    std::clog << "Could not connect" << std::endl;

    return 1;
}
